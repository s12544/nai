// naiproj.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <list>

using namespace std;
using namespace cv;

int CalculateAvg(const list<int> &list);
void on_low_r_thresh_trackbar(int, void *);
void on_high_r_thresh_trackbar(int, void *);
void on_low_g_thresh_trackbar(int, void *);
void on_high_g_thresh_trackbar(int, void *);
void on_low_b_thresh_trackbar(int, void *);
void on_high_b_thresh_trackbar(int, void *);
int low_r = 91, high_r = 179,
	low_g = 130, high_g = 190, 
	low_b = 30,  high_b = 255;

int main()
{	
	VideoCapture cap(0);
	Mat cam, threshold, firstF;
	
	namedWindow("trackbar for threshold", WINDOW_AUTOSIZE);
	
	//-- Trackbars to set thresholds for RGB values
	createTrackbar("Low R", "trackbar for threshold", &low_r, 255, on_low_r_thresh_trackbar);
	createTrackbar("High R", "trackbar for threshold", &high_r, 255, on_high_r_thresh_trackbar);
	createTrackbar("Low G", "trackbar for threshold", &low_g, 255, on_low_g_thresh_trackbar);
	createTrackbar("High G", "trackbar for threshold", &high_g, 255, on_high_g_thresh_trackbar);
	createTrackbar("Low B", "trackbar for threshold", &low_b, 255, on_low_b_thresh_trackbar);
	createTrackbar("High B", "trackbar for threshold", &high_b, 255, on_high_b_thresh_trackbar);
	
	int iLastX = -1;
	int iLastY = -1;

	//Create a black image with the size as the camera output
	cap >> firstF;
	Mat imgLines = Mat::zeros(firstF.size(), CV_8UC3);

	//create list for tracking last 10 location
	list<int> trackX;
	list<int> trackY;

	while (true) {
 
		cap >> cam;
		Mat imgHSV;
		cvtColor(cam, imgHSV, COLOR_BGR2HSV); //Convert the captured frame from BGR to HSV

		//-- Detect the object based on RGB Range Values
		inRange(imgHSV, Scalar(low_b, low_g, low_r), Scalar(high_b, high_g, high_r), threshold);

		//morphological opening (removes small objects from the foreground)
		erode(threshold, threshold, getStructuringElement(CV_SHAPE_ELLIPSE, Size(4, 4)));
		dilate(threshold, threshold, getStructuringElement(CV_SHAPE_ELLIPSE, Size(4, 4)));

		//morphological closing (removes small holes from the foreground)
		dilate(threshold, threshold, getStructuringElement(CV_SHAPE_ELLIPSE, Size(4, 4)));
		erode(threshold, threshold, getStructuringElement(CV_SHAPE_ELLIPSE, Size(4, 4)));


		Moments oMoments = moments(threshold);
		double dM01 = oMoments.m01;
		double dM10 = oMoments.m10;
		double dArea = oMoments.m00;
		
		// if the area <= 5000, I consider that the there are no object in the image and it's because of the noise, the area is not zero
		if (dArea > 10000)
		{
			//cout<<"DArea Now: "<< abs(dArea)<<endl;

			//calculate the position of the ball
			int posX = int(dM10 / dArea);
			int posY = int(dM01 / dArea);

			if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0)
			{
				//Draw a red line from the previous point to the current point
				line(imgLines, Point(posX, posY), Point(iLastX, iLastY), Scalar(0, 0, 255), 2);
			}
			//------------------------------
			{
				int avgX,avgY;
				//delete more than 10 elements
				if (trackX.size() > 10) trackX.pop_front();
				if (trackY.size() > 10) trackY.pop_front();
				//add current position to list 
				trackX.push_back(posX);
				trackY.push_back(posY);
				avgX = CalculateAvg(trackX);
				avgY = CalculateAvg(trackY);
				//cout << "X:  " << abs(posX - avgX) << ", Y:  " << abs(posY - avgY)<<endl;
				//show directions
				if (abs(posX - avgX) > abs(posY - avgY))
				{
					if (posX > avgX) cout << "prawo\n";
					else if (posX < avgX) cout << "lewo\n";
				}
				else
				{
					if (posY > avgY) cout << "gora\n";
					else cout << "dol\n";
				}

			}
			//------------------------------

			iLastX = posX;
			iLastY = posY;

		}
		//show threshold to detect object
		imshow("Object Detection", threshold);
		//add tracking on ball
		cam = cam + imgLines;
		//show camera		
		imshow("Camera", cam);
		


		if (waitKey(30) == 27) //esc to exit, (30) mean after 30ms
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
	}
	destroyWindow("trackbar for threshold");
	return 0;
}

//lower range for red
void on_low_r_thresh_trackbar(int, void *)
{
	low_r = min(high_r - 1, low_r);
	setTrackbarPos("Low R", "Object Detection", low_r);
}
//lower upper for red
void on_high_r_thresh_trackbar(int, void *)
{
	high_r = max(high_r, low_r + 1);
	setTrackbarPos("High R", "Object Detection", high_r);
}
//lower range for green
void on_low_g_thresh_trackbar(int, void *)
{
	low_g = min(high_g - 1, low_g);
	setTrackbarPos("Low G", "Object Detection", low_g);
}
//upper range for green
void on_high_g_thresh_trackbar(int, void *)
{
	high_g = max(high_g, low_g + 1);
	setTrackbarPos("High G", "Object Detection", high_g);
}
//lower range for blue
void on_low_b_thresh_trackbar(int, void *)
{
	low_b = min(high_b - 1, low_b);
	setTrackbarPos("Low B", "Object Detection", low_b);
}
//upper range for blue
void on_high_b_thresh_trackbar(int, void *)
{
	high_b = max(high_b, low_b + 1);
	setTrackbarPos("High B", "Object Detection", high_b);
}
//calculate avg
int CalculateAvg(const list<int> &list)
{
	double avg = 0;
	std::list<int>::iterator it;
	for (auto it = list.begin(); it != list.end(); it++) avg += *it;
	avg /= list.size();
	return int(avg);
}